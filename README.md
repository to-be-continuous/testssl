# GitLab CI template for Test SSL

This project implements a GitLab CI/CD template to test your TLS/SSL servers compliance with [Test SSL](https://testssl.sh/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/testssl/gitlab-ci-testssl@3.7.0
    # 2: set/override component inputs
    inputs:
      args: "--severity HIGH" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/testssl'
    ref: '3.7.0'
    file: '/templates/gitlab-ci-testssl.yml'

variables:
  # 2: set/override template variables
  TESTSSL_ARGS: "--severity HIGH" # ⚠ this is only an example
```

:warning: this job do not fail unless there is a technical problem while scanning your endpoint. This means you have to read the tool report on gitlab or download the report to properly assert if the security level of your endpoint is correct.

## `testssl` job

This job performs a TLS/SSL compliancy analysis on a given server.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------- | ---------------------------------------- | ----------------- |
| `image` / `TESTSSL_IMAGE` | The Docker image used to run [Test SSL](https://testssl.sh/) | `registry.hub.docker.com/drwetter/testssl.sh:latest` <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-TESTSSL_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-TESTSSL_IMAGE) |
| `args` / `TESTSSL_ARGS` | Test SSL [command-line options](https://testssl.sh/#usage)   | `--severity MEDIUM` |
| `url` / `TESTSSL_URL` | Server url to test TLS/SSL against       | _none_ (auto evaluated: see below) |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Test SSL tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/testssl.native.json` | testssl.sh JSON format | [DefectDojo integration](https://docs.defectdojo.com/en/connecting_your_tools/parsers/file/testssl/) |

### test url auto evaluation

By default, the Test SSL template tries to auto-evaluates the server url to test by looking either for a
`$environment_url` variable or for an `environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Test SSL will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Test SSL tests.

If you're not using a smart deployment job, you may still explicitly declare the `TESTSSL_URL` variable (but that
will be unfortunately hardcoded to a single server).

### Hook scripts

The Test SSL template supports _optional_ **hook scripts** from your project, located in the root directory to perform additional project-specific logic:

* `pre-testssl.sh` is executed **before** running Test SSL,
* `post-testssl.sh` is executed **after** running Test SSL (whichever the tests status).
